#! /bin/bash
osd=/home/salvix/dev/scripts/scripts/osd.sh
battery=`acpi|grep Discharging|cut -d',' -f2|cut -d' ' -f2`

if [[ "${battery}" =~ 1[0-9]% ]]; then
	`$osd "$battery, please plug in the charger!"`
else
	`$osd "$battery Fine"`
fi

