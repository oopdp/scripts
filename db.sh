#! /bin/sh

now=`date +%Y-%m-%d-%H-%M-%S`

mysql_execute(){
  local t_user=$1
  local t_pwd=$2
  local t_db=$3
  local t_host=$4
  local t_conn='mysql -u'$t_user' -p'$t_pwd' -D'$t_db' -h'$t_host' -e "$5" -s'
  eval $t_conn
}

mysql_execute_nodb(){
  local t_user=$1
  local t_pwd=$2
  local t_conn='mysql -u'$t_user' -p'$t_pwd' -e "$3" -s'
  eval $t_conn
}

usage(){
	echo "HELP"
}


create_database(){
  
  echo "Creating a new DB...please provide some vital information."
  echo "Enter requested values followed by [ENTER]"
  echo "Mysql User with power to create a db:"
  read l_user
  echo "Mysql pwd for previous user:"
  read l_pwd
  echo "NEW DB name:"
  read l_new_db
  echo "NEW DB user:"
  read l_new_user
  echo "NEW DB pwd"
  read l_new_pwd
  
  mysql_execute_nodb $l_user $l_pwd "create database "$l_new_db
  mysql_execute_nodb $l_user $l_pwd "grant all on "$l_new_db"."*" to "$l_new_user"@localhost identified by '$l_new_pwd'"
}






case "$1" in
'create-database')
  create_database
  ;;
#~ Not matched command
*)
	usage
	;;
esac
exit 0
