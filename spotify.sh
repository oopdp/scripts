#! /bin/sh
# see https://community.spotify.com/t5/Desktop-Linux/Basic-controls-via-command-line/td-p/4295625

usage() {
    echo "toggle - play/pause"
    echo "next - next song"
    echo "prev - prev song"
}

toggle() {
    dbus-send --print-reply --dest=org.mpris.MediaPlayer2.spotify /org/mpris/MediaPlayer2 org.mpris.MediaPlayer2.Player.PlayPause
}

next() {
    dbus-send --print-reply --dest=org.mpris.MediaPlayer2.spotify /org/mpris/MediaPlayer2 org.mpris.MediaPlayer2.Player.Next
}

prev() {
    dbus-send --print-reply --dest=org.mpris.MediaPlayer2.spotify /org/mpris/MediaPlayer2 org.mpris.MediaPlayer2.Player.Previous
}

case "$1" in
'toggle')
    toggle
    ;;
'next')
    next
    ;;
'prev')
    prev
    ;;
*)
	usage
	;;
esac

exit 0
