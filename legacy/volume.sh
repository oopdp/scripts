#! /bin/sh

CONTROL='Master'
COLOR='red'
OSD_CLEAN='killall osd_cat >/dev/null 2>/dev/null'
VOL_UP='amixer set "Master" 10%+'
VOL_DOWN='amixer set "Master" 10%-'
FONT=-adobe-helvetica-bold-r-normal-*-34-240-100-100-p-182-iso8859-1
DURATION=5

usage(){
	echo "Usage $0 {+|-|toggle}"
}

osd(){
	local var=`ps -e|grep "$p"|wc -l`
	
	if [ $var -gt 0 ] 
	then
		eval $OSD_CLEAN
	fi
	
	if [ -z "$1" ]
	then
		string="Nothing to print"
	else
		string=$1
	fi
	
	if [ -z "$2" ]
	then
		delay=5
	else
		delay=$2
	fi
	
	echo "$string" | osd_cat \
		--align=center \
		--pos=bottom \
		--offset=-50 \
		--color=${COLOR} \
		--delay=${DURATION} \
		--font=${FONT} \
		--shadow=1 & 
}

data(){
	amixer get 'Master' | grep "Right:"
}
volume(){
	percentage=`data | cut -f2 -d'[' | cut -f1 -d'%'`
	echo $percentage	
}

mute_unmute(){
	local status=`data|cut -f8 -d' '`
	#echo $status
	
	if [ "$status" = "[on]" ] ; then
		amixer sset $CONTROL mute >/dev/null
		osd "Volume muted..."
	else
		amixer sset $CONTROL unmute >/dev/null
		osd_volume
	fi
}

osd_volume(){
	volume
	local var=`ps -e|grep "$p"|wc -l`
	
	if [ $var -gt 0 ] 
	then
		eval $OSD_CLEAN
	fi
	osd_cat \
		--barmode percentage\
		--align=center \
		--pos=bottom \
		--offset=+50 \
		--color=${COLOR} \
		--delay=${DURATION} \
		--shadow=1 \
		--lines=1 \
		--percentage=$percentage \
		--font=${FONT} \
		--text="Volume "$percentage"%" &
}

up(){
	eval $VOL_UP
	osd_volume
}

down(){
	eval $VOL_DOWN
	osd_volume
}

case "$1" in
'+' | 'up')
	up
	;;
'-' | 'down')
	down
	;;
'toggle')
	mute_unmute
	;;
*)
	usage
	;;
esac

exit 0
