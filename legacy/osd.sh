#! /bin/sh

FONT=-adobe-helvetica-bold-r-normal-*-34-240-100-100-p-182-iso8859-1
DURATION=5

echo "$1"|osd_cat --align=center --pos=bottom --offset=-50 --color=red --delay=$DURATION --font=$FONT &
